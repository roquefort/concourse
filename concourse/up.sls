/src/concourse/docker-compose.yml: 
  file.managed: 
    - template: jinja
    - source: salt://concourse/docker-compose.{{ saltenv }}.yml

compose-up:
  cmd.run:
    - name: >
        docker-compose up -d
    - cwd: /src/concourse
    - require:
      - cmd: compose-pull