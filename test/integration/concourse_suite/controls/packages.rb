# frozen_string_literal: true

control 'package-01' do
  impact 1.0
  title 'Do not run deprecated inetd or xinetd'
  desc 'http://www.nsa.gov/ia/_files/os/redhat/rhel5-guide-i731.pdf, Chapter 3.2.1'
  describe package('inetd') do
    it { should_not be_installed }
  end
  describe package('xinetd') do
    it { should_not be_installed }
  end
end