# frozen_string_literal: true

control "instances" do
  impact 0.6
  title 'Server: Configure the service port'
  desc 'Always specify which port the SSH server should listen.'
  desc 'rationale', 'This ensures that there are no unexpected settings' # Requires InSpec >=2.3.4
  tag 'ssh','sshd','openssh-server'
  tag cce: 'CCE-27072-8'
  ref 'NSA-RH6-STIG - Section 3.5.2.1', url: 'https://www.nsa.gov/ia/_files/os/redhat/rhel5-guide-i731.pdf'


  describe "a2concourse01" do
    subject do
      json({ command: 'aws ec2 describe-instances --query "Reservations[0].Instances[0]" --filters "Name=tag:Name,Values=a2concourse01"'})
    end
    
    its(['State', 'Name']) { should eq 'running' }
  end

  describe "a2concourse02" do
    subject do
      json({ command: 'aws ec2 describe-instances --query "Reservations[0].Instances[0]" --filters "Name=tag:Name,Values=a2concourse02"'})
    end
    
    its(['State', 'Name']) { should eq 'running' }
  end
end