variable "region" {
}

variable "environment" {
}

variable "datacenter" {
}

variable "private_key" {
}

variable "public_key" {
}

variable "identifier" {
  type = "map"
  default = {
    "instance1" = "01"
    "instance2" = "02"
  }
}