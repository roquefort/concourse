output "static_terraform_output" {
  description = <<EOD
This output is used as an attribute in the inspec_attributes control
EOD

  value = "static_terraform_output"
}

output "cluster_name" {
  value = "${module.concourse.fqdn}"
}