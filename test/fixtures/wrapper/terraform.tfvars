region      = "us-east-1"

datacenter  = "midway"

private_key = "../../../assets/keys/id_rsa"

public_key  = "../../../assets/keys/id_rsa.pub"