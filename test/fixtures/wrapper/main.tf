module "concourse" {
  source         = "../../../"
  compose_source = "../../../"
  environment    = "${var.environment}"
  region         = "${var.region}"
  datacenter     = "${var.datacenter}"
  private_key    = "${var.private_key}"
  public_key     = "${var.public_key}"
  identifier     = "${var.identifier}"
}
