output "fqdn" {
  value = {
    for instance in aws_route53_record.main:
    instance.id => instance.fqdn
  } 
}