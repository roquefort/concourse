provider "aws" {
  region = var.region
}

locals {
  environment = var.environment
  datacenter  = var.datacenter
  identifier  = var.identifier

  dns_prefix  = "${lookup(var.environment_shortname, local.environment)}${lookup(var.datacenter_shortname, local.datacenter)}${var.service}"
}

data "aws_route53_zone" "main" {
  name         = var.domain_name
  private_zone = false
}

data "aws_ami" "main" {
  most_recent = true

  filter {
    name   = "name"
    values = ["stop-concourse"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["836283262781"] # Me
}

resource "aws_security_group" "main" {
  name        = "${local.dns_prefix}"
  description = "Allow SSH and web traffic"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "main" {
  key_name_prefix   = "${local.dns_prefix}"
  public_key        = "${file(var.public_key)}"
}

resource "aws_instance" "main" {
  for_each = var.identifier
  
  ami           = data.aws_ami.main.id
  instance_type = "t2.medium"

  key_name               = aws_key_pair.main.key_name
  vpc_security_group_ids = [aws_security_group.main.id]

  provisioner "file" {
    source      = "${var.compose_source}docker-compose.yml"
    destination = "/tmp/docker-compose.yml"
  }

  provisioner "remote-exec" {
    inline = [
      "docker-compose -f /tmp/docker-compose.yml up -d"
    ]
  }

  connection {
    host        = self.public_ip
    type        = "ssh"
    user        = "centos"
    private_key = "${file(var.private_key)}"
  }

  tags = {
    Name        = "${local.dns_prefix}${each.value}"
    Instance    = each.value
    Environment = local.environment
    Datacenter  = local.datacenter
  }
}

resource "aws_route53_record" "main" {
  for_each = var.identifier

  zone_id = data.aws_route53_zone.main.zone_id
  type    = "CNAME"
  ttl     = "30"

  name    = "${local.dns_prefix}${each.value}"
  records = [aws_instance.main[each.key].public_dns]
}