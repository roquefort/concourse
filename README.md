# concourse

## Problems
- In the tutorial, workers share the same volume as the Docker database. This makes the worker cache volume data, which causes errors. We need to configure separate workers that are ephemeral.

## Commands

### View all available commands (which may also be set as environment variables)
docker run -it concourse/concourse:5.4 --help

### Use fly to create a target alias pointed at the Concourse API
fly --target securus login --concourse-url http://127.0.0.1:8080 -u admin -p admin
fly --target securus sync

### Update the pipeline
fly -t securus set-pipeline -c pipeline.yml -p docker

### Watch a job
fly -t securus watch -j hello-world/job-hello-world

### Run full test cycle
kitchen test

## Testing

### Bring up test infrastructure
kitchen create
kitchen converge
kitchen setup

### Test the test infrastructure
kitchen verify

### Destroy the test infrastructure
kitchen destroy

### Do all the things
kitchen test