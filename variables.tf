variable "region" {
  default = "us-east-1"
}

variable "service" {
  default = "concourse"
}

variable "environment" {
  default = "development"
}

variable "environment_shortname" {
  type    = "map"
  default = {
    "development" = "d"
    "alpha"       = "a"
    "integrate"   = "i"
    "reseller"    = "r"
    "production"  = "c"
  }
}

variable "datacenter" {
  default = "midway"
}

variable "datacenter_shortname" {
  type    = "map"
  default = {
    "atlanta" = "1"
    "midway"  = "2"
  }
}

variable "domain_name" {
  default = "corebox.io"
}

variable "identifier" {
  type = "map"
  default = {
    "instance1" = "01"
  }
}

variable "private_key" {
  default = "./assets/keys/id_rsa"
}

variable "public_key" {
  default = "./assets/keys/id_rsa.pub"
}

variable "compose_source" {
  default = "./"
}